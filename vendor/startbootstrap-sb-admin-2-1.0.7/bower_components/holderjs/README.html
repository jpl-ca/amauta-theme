<h1 id="holder">Holder</h1>
<p><img src="http://imsky.github.io/holder/images/header.png" alt=""></p>
<p>Holder renders image placeholders on the client side using SVG.</p>
<p>Used by <a href="http://getbootstrap.com">Bootstrap</a>, thousands of <a href="https://github.com/search?q=holder.js+in%3Apath&amp;type=Code&amp;ref=searchresults">open source projects</a>, and <a href="https://search.nerdydata.com/search/#!/searchTerm=holder.js/searchPage=1/sort=pop">many other sites</a>.</p>
<h2 id="installing">Installing</h2>
<ul>
<li><a href="http://bower.io/">Bower</a>: <code>bower install holderjs</code></li>
<li><a href="http://cdnjs.com/">cdnjs</a>: <a href="http://cdnjs.com/libraries/holder">http://cdnjs.com/libraries/holder</a></li>
<li><a href="http://www.jsdelivr.com">jsDelivr</a>: <a href="http://www.jsdelivr.com/#!holder">http://www.jsdelivr.com/#!holder</a></li>
<li><a href="https://rails-assets.org">Rails Assets</a>: <code>gem &#39;rails-assets-holderjs&#39;</code></li>
<li><a href="http://atmospherejs.com/">Meteor</a>: <code>mrt add holder</code></li>
<li><a href="https://packagist.org/">Composer</a>: <code>php composer.phar update imsky/holder</code></li>
<li><a href="http://www.nuget.org/">NuGet</a>: <code>Install-Package Holder.js</code></li>
</ul>
<h2 id="usage">Usage</h2>
<p>Include <code>holder.js</code> in your HTML:</p>
<pre><code class="lang-html">&lt;script src=&quot;holder.js&quot;&gt;&lt;/script&gt;
</code></pre>
<p>Holder will then process all images with a specific <code>src</code> attribute, like this one:</p>
<pre><code class="lang-html">&lt;img src=&quot;holder.js/200x300&quot;&gt;
</code></pre>
<p>The above tag will render as a placeholder 200 pixels wide and 300 pixels tall.</p>
<p>To avoid console 404 errors, you can use <code>data-src</code> instead of <code>src</code>.</p>
<h2 id="themes">Themes</h2>
<p><img src="http://imsky.github.io/holder/images/holder_sky.png" alt=""><img src="http://imsky.github.io/holder/images/holder_vine.png" alt=""><img src="http://imsky.github.io/holder/images/holder_lava.png" alt=""></p>
<p>Holder includes support for themes, to help placeholders blend in with your layout.</p>
<p>There are 6 default themes: <code>sky</code>, <code>vine</code>, <code>lava</code>, <code>gray</code>, <code>industrial</code>, and <code>social</code>. Use them like so:</p>
<pre><code class="lang-html">&lt;img src=&quot;holder.js/200x300/sky&quot;&gt;
</code></pre>
<h2 id="custom-colors">Custom colors</h2>
<p>Custom colors on a specific image can be specified in the <code>background:foreground</code> format using hex notation, like this:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/100x200/#000:#fff&quot;&gt;
</code></pre>
<p>The above will render a placeholder with a black background and white text.</p>
<h2 id="custom-text">Custom text</h2>
<p>You can specify custom text using the <code>text:</code> operator:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/200x200/text:hello world&quot;&gt;
</code></pre>
<p>If you have a group of placeholders where you&#39;d like to use particular text, you can do so by adding a <code>text</code> property to the theme:</p>
<pre><code class="lang-js">Holder.addTheme(&quot;thumbnail&quot;, { background: &quot;#fff&quot;, text: &quot;Thumbnail&quot; });
</code></pre>
<p>Holder automatically adds line breaks to text that goes outside of the image boundaries. If the text is so long it goes out of both horizontal and vertical boundaries, the text is moved to the top. If you prefer to control the line breaks, you can add <code>\n</code> to the <code>text</code> property:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/300x200/text:Add \n line breaks \n anywhere.&quot;&gt;
</code></pre>
<h2 id="custom-fonts-web-fonts-and-icon-fonts">Custom fonts, web fonts and icon fonts</h2>
<p>You can set a placeholder&#39;s font either through a theme or through the <code>font</code> flag:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/300x200/font:Helvetica&quot;&gt;
</code></pre>
<p>Placeholders using a custom font are rendered using canvas by default, due to SVG&#39;s constraints on cross-domain resource linking. If you&#39;re using only locally available fonts, you can disable this behavior by setting <code>noFontFallback</code> to <code>true</code> in <code>Holder.run</code> options. However, if you need to render a SVG placeholder using an externally loaded font, you have to use the <code>object</code> tag instead of the <code>img</code> tag and add a <code>holderjs</code> class to the appropriate <code>link</code> tags. Here&#39;s an example:</p>
<pre><code class="lang-html">&lt;head&gt;
&lt;link href=&quot;http://.../font-awesome.css&quot; rel=&quot;stylesheet&quot; class=&quot;holderjs&quot;&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;object data=&quot;holder.js/300x200/font:FontAwesome&quot;&gt;&lt;/object&gt;
</code></pre>
<p><strong>Important:</strong> When testing locally, font URLs must have a <code>http</code> or <code>https</code> protocol defined.</p>
<p><code>&lt;object&gt;</code> placeholders work like <code>&lt;img&gt;</code> placeholders, with the added benefit of their DOM being able to be inspected and modified.</p>
<h2 id="customizing-themes">Customizing themes</h2>
<p>Themes have 5 properties: <code>foreground</code>, <code>background</code>, <code>size</code>, <code>font</code> and <code>fontweight</code>. The <code>size</code> property specifies the minimum font size for the theme. The <code>fontweight</code> default value is <code>bold</code>. You can create a sample theme like this:</p>
<pre><code class="lang-js">Holder.addTheme(&quot;dark&quot;, {
  background: &quot;#000&quot;,
  foreground: &quot;#aaa&quot;,
  size: 11,
  font: &quot;Monaco&quot;,
  fontweight: &quot;normal&quot;
});
</code></pre>
<h2 id="using-custom-themes">Using custom themes</h2>
<p>There are two ways to use custom themes with Holder:</p>
<ul>
<li>Include theme at runtime to render placeholders already using the theme name</li>
<li>Include theme at any point and re-render placeholders that are using the theme name</li>
</ul>
<p>The first approach is the easiest. After you include <code>holder.js</code>, add a <code>script</code> tag that adds the theme you want:</p>
<pre><code class="lang-html">&lt;script src=&quot;holder.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
Holder.addTheme(&quot;bright&quot;, {
  background: &quot;white&quot;, foreground: &quot;gray&quot;, size: 12
});
&lt;/script&gt;
</code></pre>
<p>The second approach requires that you call <code>run</code> after you add the theme, like this:</p>
<pre><code class="lang-js">Holder.addTheme(&quot;bright&quot;, {background: &quot;white&quot;, foreground: &quot;gray&quot;, size: 12}).run();
</code></pre>
<h2 id="using-custom-themes-and-domain-on-specific-images">Using custom themes and domain on specific images</h2>
<p>You can use Holder in different areas on different images with custom themes:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;example.com/100x100/simple&quot; id=&quot;new&quot;&gt;
</code></pre>
<pre><code class="lang-js">Holder.run({
  domain: &quot;example.com&quot;,
  themes: {
    &quot;simple&quot;: {
      background: &quot;#fff&quot;,
      foreground: &quot;#000&quot;,
      size: 12
    }
  },
  images: &quot;#new&quot;
});
</code></pre>
<h2 id="random-themes">Random themes</h2>
<p>You can render a placeholder with a random theme using the <code>random</code> flag:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/300x200/random&quot;&gt;
</code></pre>
<h2 id="fluid-placeholders">Fluid placeholders</h2>
<p>Specifying a dimension in percentages creates a fluid placeholder that responds to media queries.</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/100%x75/social&quot;&gt;
</code></pre>
<p>By default, the fluid placeholder will show its current size in pixels. To display the original dimensions, i.e. 100%x75, set the <code>textmode</code> flag to <code>literal</code> like so: <code>holder.js/100%x75/textmode:literal</code>.</p>
<p>Fluid placeholders need to be visible in order to work. In cases when a placeholder is not visible, the <code>Holder.invisibleErrorFn</code> function is called, which takes the callee function as an argument and returns a function that takes the placeholder element as an argument. This function by default throws an exception, however its behavior can and should be overridden by the user.</p>
<h2 id="automatically-sized-placeholders">Automatically sized placeholders</h2>
<p>If you&#39;d like to avoid Holder enforcing an image size, use the <code>auto</code> flag like so:</p>
<pre><code class="lang-html">&lt;img data-src=&quot;holder.js/200x200/auto&quot;&gt;
</code></pre>
<p>The above will render a placeholder without any embedded CSS for height or width.</p>
<p>To show the current size of an automatically sized placeholder, set the <code>textmode</code> flag to <code>exact</code> like so: <code>holder.js/200x200/auto/textmode:exact</code>.</p>
<h2 id="background-placeholders">Background placeholders</h2>
<p>Holder can render placeholders as background images for elements with the <code>holderjs</code> class, like this:</p>
<pre><code class="lang-css">#sample {background:url(?holder.js/200x200/social) no-repeat}
</code></pre>
<pre><code class="lang-html">&lt;div id=&quot;sample&quot; class=&quot;holderjs&quot;&gt;&lt;/div&gt;
</code></pre>
<p>The Holder URL in CSS should have a <code>?</code> in front. Like in image placeholders, you can specify the Holder URL in a <code>data-background-src</code> attribute:</p>
<pre><code class="lang-html">&lt;div data-background-src=&quot;?holder.js/300x200&quot;&gt;&lt;/div&gt;
</code></pre>
<p><strong>Important:</strong> Make sure to define a height and/or width for elements with background placeholders. Fluid background placeholders are not yet supported.</p>
<p><strong>Important:</strong> Some browsers can&#39;t parse URLs like <code>?holder.js/300x200/#fff:#000</code> due to the <code>#</code> characters. You can use <code>^</code> in place of <code>#</code> like this: <code>?holder.js/300x200/^fff:^000</code>.</p>
<h2 id="custom-settings">Custom settings</h2>
<p>Holder extends its default settings with the settings you provide, so you only have to include those settings you want changed. For example, you can run Holder on a specific domain like this:</p>
<pre><code class="lang-js">Holder.run({domain:&quot;example.com&quot;});
</code></pre>
<h2 id="using-custom-settings-on-load">Using custom settings on load</h2>
<p>You can prevent Holder from running its default configuration by executing <code>Holder.run</code> with your custom settings right after including <code>holder.js</code>. However, you&#39;ll have to execute <code>Holder.run</code> again to render any placeholders that use the default configuration.</p>
<h2 id="inserting-an-image-with-optional-custom-theme">Inserting an image with optional custom theme</h2>
<p>You can add a placeholder programmatically by chaining Holder calls:</p>
<pre><code class="lang-js">Holder.addTheme(&quot;new&quot;, {
  foreground: &quot;#ccc&quot;,
  background: &quot;#000&quot;,
  size: 10
}).addImage(&quot;holder.js/200x100/new&quot;, &quot;body&quot;).run();
</code></pre>
<p>The first argument in <code>addImage</code> is the <code>src</code> attribute, and the second is a CSS selector of the parent element.</p>
<h2 id="using-different-renderers">Using different renderers</h2>
<p>Holder has three renderers: canvas, SVG, and HTML. The SVG renderer is used by default, however you can set the renderer using the <code>renderer</code> option, with either <code>svg</code>, <code>canvas</code>, or <code>html</code> values.</p>
<pre><code class="lang-js">Holder.run({renderer: &#39;canvas&#39;});
</code></pre>
<h2 id="using-with-lazyload-js-https-github-com-tuupola-jquery_lazyload-">Using with <a href="https://github.com/tuupola/jquery_lazyload">lazyload.js</a></h2>
<p>Holder is compatible with <code>lazyload.js</code> and works with both fluid and fixed-width images. For best results, run <code>.lazyload({skip_invisible:false})</code>.</p>
<h2 id="using-with-angular-js">Using with Angular.js</h2>
<p>You can use Holder in Angular projects with the following JS and HTML code (by <a href="https://github.com/NickClark">Nick Clark</a>):</p>
<pre><code class="lang-js">angular.module(&#39;MyModule&#39;).directive(&#39;myHolder&#39;, function() {
  return {
    link: function(scope, element, attrs) {
      attrs.$set(&#39;data-src&#39;, attrs.myHolder);
      Holder.run({images:element[0]});
    }
  };
});
</code></pre>
<pre><code class="lang-html">&lt;img my-holder=&quot;holder.js/200x300&quot;&gt;
</code></pre>
<h2 id="browser-support">Browser support</h2>
<ul>
<li>Chrome</li>
<li>Firefox 3+</li>
<li>Safari 4+</li>
<li>Internet Explorer 9+ (with partial support for 6-8)</li>
<li>Opera 15+ (with partial support for 12)</li>
<li>Android (with fallback)</li>
</ul>
<h2 id="license">License</h2>
<p>Holder is provided under the <a href="http://opensource.org/licenses/MIT">MIT License</a>.</p>
<h2 id="credits">Credits</h2>
<p>Holder is a project by <a href="http://imsky.co">Ivan Malopinsky</a>.</p>
